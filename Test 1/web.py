#Test1
import requests

url = "https://httpbin.org/anything"
params = {'msg': 'welcomeuser', 'isadmin': 1}
header = {'User-Agent': "Mozilla 5.0 (Linux; Android9; RMX1821) AppleWebKit/527.36 (KHTML, like Gecko) Chrome 76.0.3809.132 Mobile Safari 527.36"}

def req(url, params, header=''):
    if header :             
        r = requests.post(url, params, headers=header)
        return r.request.headers
    else:
        r = requests.post(url, params)
        # return r.request.body
        return r.content

print(' - The Response Body : ')
print( req(url, params) )
print(' - The Request Header : ')
print(req(url, params, header))