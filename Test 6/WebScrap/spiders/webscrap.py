import scrapy
from selenium import webdriver
from scrapy.http import HtmlResponse

class ProductItem(scrapy.Item):
    name = scrapy.Field()

class ProductSpider(scrapy.Spider):
    name = "product_spider"
    allowed_domains = ['www.woolworths.com.au']
    start_urls = ['https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas']

  
    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')

        self.driver = webdriver.Chrome(chrome_options=options)

        
    def parse(self, response):
        self.driver.get(response.url)

        # html = HtmlResponse(url="my HTML string", body=self.driver.page_source, encoding="utf-8")
        # test = html.selector.xpath('/html/body/div[7]/div[1]/ul/li/div[2]/a/@href').extract()

        breadcrumps = self.driver.find_element_by_class_name('breadcrumbs-linkList')

        linkList = breadcrumps.text
        ListStr = str(linkList)
        res_breadcrumps = ListStr.splitlines()

        product_list = []
        for title in self.driver.find_elements_by_xpath('//*[@class="shelfProductTile-description" or @class="shelfProductTile-descriptionLink"]'):
            item = ProductItem()
            item["name"] = title.text
            product_list.append(item)
            

        yield {
                'breadcrumps': res_breadcrumps,
                'products': product_list,
            }

        self.driver.close()
