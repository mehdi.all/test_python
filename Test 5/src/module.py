
import simplejson as json
import csv

# import the data.json
with open('../data.json', 'r') as f:
    distros_dict = json.load(f)

     
def InitCSV(path):
    #Clean file
    f = open(path, "w")
    f.truncate()
    f.close()
    #Init the csv with header
    with open(path, 'a') as csvFile:
        header = ['Id', 'Name', 'Price']
        writer = csv.writer(csvFile)
        writer.writerow(header)
    csvFile.close()
        
InitCSV("../products.csv")

def SaveSvg(row):
    with open('../products.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(row)
    csvFile.close()
    

def isAvailable(val):
    if val['IsAvailable']:
        print(' You Can Buy', val['Name'][:31] ,' at our store at ',round(val['Price'], 2)  )
        SaveSvg([ val['Stockcode'] , val['Name'][:31], round(val['Price'], 2) ])
    else:
        print(' > The product is unavailable, Product id ',val['Stockcode'],' Product Name ', val['Name'][:31] )

for distro in distros_dict['Bundles']:   
    try:
        for p_id, p_info in distro.items():
            if p_id in "Name":
                name = distro[p_id][:31]
            elif p_id == "Products" or p_id == "Product":
                try: 
                    isAvailable(distro[p_id][0])
                except:
                    try:
                        isAvailable(distro[p_id][1])
                    except:
                        print(' #Error')
            else:
                print(' #Error')

    except:
        print(" #Error")
    
    

    
    

