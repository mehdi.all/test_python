
from functools import reduce
from operator import mul
from itertools import groupby


list2 = [0,2,5,"a",6,'test']

list4_1 = [0, 7, 7, 7, 5, 4, 9, 9, 0]
list4_2 = [4, 4, 5, 6, 8, 8, 8]
list4_3 = [-5, -5, 7, 7, 12, 0]


#Test2
def sum_int(list):
    total = 0
    for x in list:
        if isinstance(x, int):
            total += x
    return total


#Test3
def persistence(n):
    times = 0
    while n > 9:
        n = reduce(lambda x, y: x * y, [int(i) for i in str(n)])
        times += 1
    return times

def persistence2(number, count=0):
    # exit once the number is less than 10
    if number < 10:
        return count
    # get new number by multiplying digits of a number
    new_number = reduce(mul, map(int, str(number)))
    return persistence2(new_number, count + 1)

#Test4
def sum_consecutives(list):
    # i use the library groupby to get recursivity and multply it by the number
    return [k * sum(1 for i in g) for k, g in groupby(list)]


print('-Test-2-------------')
print(' sum_int(',list2,') = ', sum_int(list2))

print('-Test-3----persistence---')
print('persistence(39) -> ', persistence(39))
print('persistence2(999) ->', persistence2(999))
print('persistence2(4) -> ', persistence2(4))

print('-Test-4-------------')
print (sum_consecutives(list4_1))
print (sum_consecutives(list4_2))
print (sum_consecutives(list4_3))
